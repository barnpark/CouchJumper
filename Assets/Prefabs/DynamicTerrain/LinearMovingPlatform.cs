﻿using Scripts;
using System.Collections;
using UnityEngine;

public class LinearMovingPlatform : MonoBehaviour {

    #region Public Properties
    /// <summary>
    ///     The current direction and speed of the platform.
    /// </summary>
    [Tooltip("The initial direction and speed of the platform.")]
    public Vector2 Direction = new Vector2(1, 0);

    /// <summary>
    ///     The amount of time in seconds to pause after bumping into something before
    ///     reversing direction.
    /// </summary>
    [Tooltip("The amount of time in seconds to pause after bumping into something before reversing direction.\n" +
             "Setting this to 0 will disable the pause.")]
    public float BumpWaitTime = 2;

    /// <summary>
    ///     The amount of time in seconds it takes the platform to get to full speed after
    ///     switching direction.
    /// </summary>
    [Tooltip("The amount of time in seconds it takes the platform to get to full speed after switching direction.\n" +
             "Setting this to 0 will move the platform to full speed instantly.")]
    public float AccelerationTime = 1;
    #endregion

    #region Private Fields
    private Rigidbody2D _rigidBody;
    private bool _bumpWaitActive;
    #endregion

    #region Hooks
    private void Start() {
        _rigidBody = GetComponent<Rigidbody2D>();
        _rigidBody.velocity = Direction;
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (_bumpWaitActive || other.gameObject.layer != LayerMask.NameToLayer("StaticTerrain"))
            return;

        transform.position -= (Vector3) _rigidBody.velocity * Time.fixedDeltaTime;
        _rigidBody.velocity = Vector2.zero;
        Direction = -Direction;

        if (BumpWaitTime.ApproximatelyEqualTo(0)) {
            BeginMoving();
        } else {
            _bumpWaitActive = true;
            StartCoroutine(WaitThenMove());
        }
    }
    #endregion

    #region Private Methods

    private void BeginMoving() {
        _bumpWaitActive = false;
        if (AccelerationTime.ApproximatelyEqualTo(0)) {
            _rigidBody.velocity = Direction;
        } else {
            StartCoroutine(EaseToTargetDirection());
        }
    }
    #endregion

    #region Coroutines

    private IEnumerator WaitThenMove() {
        yield return new WaitForSeconds(BumpWaitTime);
        BeginMoving();
    }

    private IEnumerator EaseToTargetDirection() {
        while (true) {
            if (_rigidBody.velocity.sqrMagnitude >= Direction.sqrMagnitude) {
                _rigidBody.velocity = Direction;
                yield break;
            }
            _rigidBody.velocity += Direction * Time.fixedDeltaTime / AccelerationTime;
            yield return new WaitForFixedUpdate();
        }
    }
    #endregion

}
