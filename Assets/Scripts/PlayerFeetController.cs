﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFeetController : MonoBehaviour {

    public bool DetectCollisions { set; get; }

    private PlayerController _playerController;


	void Start () {
	    _playerController = this.GetComponentInParent<PlayerController>();
	}
	

    private void OnCollisionExit2D(Collision2D other) {
        _playerController.canJump = false;
        DetectCollisions = false;
    }
}
