﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    #region Private Fields

    private Camera _camera;

    private const float TargetAspectRatio = 4f / 3;

    #endregion

    #region Hooks

    private void Start() {
        _camera = GetComponent<Camera>();
    }

    private void LateUpdate() {
        var scalingFactor = (float) Screen.width / Screen.height / TargetAspectRatio;

        if (scalingFactor < 1) {
            // Letterbox
            var rect = _camera.rect;
            rect.width = 1;
            rect.height = scalingFactor;
            rect.x = 0;
            rect.y = (1 - scalingFactor) / 2;
            _camera.rect = rect;
        } else {
            // Pillarbox
            var rect = _camera.rect;
            rect.width = 1 / scalingFactor;
            rect.height = 1;
            rect.x = (1 - 1 / scalingFactor) / 2;
            rect.y = 0;
            _camera.rect = rect;
        }
    }

    #endregion
}
