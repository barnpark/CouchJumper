﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour {

    public bool canJump { get; set; }

    public int speed = 10;
    public int jumpSpeed = 400;

    private Rigidbody2D rigidBody;
    private PlayerFeetController feetController;


	void Start () {
	    rigidBody = GetComponent<Rigidbody2D>();
	    feetController = GetComponentInChildren<PlayerFeetController>();
	}
	
	void FixedUpdate () {

	    var x = Input.GetAxis("Horizontal");
	    var y = Input.GetAxis("Vertical");

	    if (x > 0) {
            rigidBody.AddForce(Vector2.right * speed);
        } else if(x < 0) {
	        rigidBody.AddForce(Vector2.left * speed);
        }

	    if (y > 0 && canJump) {
	        feetController.DetectCollisions = false;
            rigidBody.AddForce(Vector2.up * jumpSpeed);
        }
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.otherCollider is CapsuleCollider2D) {
            feetController.DetectCollisions = true;
            canJump = true;
        }
    }

}
