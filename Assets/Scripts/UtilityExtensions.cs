﻿namespace Scripts {
    public static class UtilityExtensions {
        public static bool ApproximatelyEqualTo(this float from, float target, float tolerance = 1e-6f) {
            return target - tolerance <= from && from <= target + tolerance;
        }
    }
}
